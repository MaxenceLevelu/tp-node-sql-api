const express = require('express')
const app = express()
const axios = require('axios');
const mysql = require('mysql');
const apikey = '301a55dc'

const con = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "root",
    database: "moviedata"
});

con.connect();

app.set('view engine', 'ejs');

app.use(express.static("public"));

app.use(express.json())
app.use(express.urlencoded({
    extended: true
}))

app.all('/', function (req, res, next) {
    console.log('Bienvuenue dans all')
    next()
})

app.post('/users', function (req, res) {
    const sql = `INSERT INTO users VALUES (NULL, '${req.body.pseudo}', '${req.body.email}', '${req.body.firstname}', '${req.body.lastname}', '${req.body.createdAt}', ${req.body.updatedAt})`;
    con.query(sql, function (err, result) {
        if (err) throw err;
        console.log("1 record inserted");
    });
})

app.get('/users/:id', function (req, res) {
    con.query(`SELECT * FROM users WHERE id=${req.params.id}`, (err, result, field) => {
        if (err) throw err
        res.render('pages/index', {
            result: result
        })
    })
})

app.get('/users/:id/movies', function (req, res) {
    con.query(`SELECT * FROM movies_users JOIN users ON users.id = movies_users.users_id WHERE users_id=${req.params.id}`, async (err, result, fields) => {
        if (err) throw err      
        const movies = []   

        for (let index = 0; index < result.length; index++) {
            const element = result[index].movie_id;            
            const { data } = await axios.get(`http://www.omdbapi.com/?i=${element}&apikey=${apikey}`)
            movies.push(data)
        }
        console.log(movies)
        res.render('pages/movie', {
            movies: movies,
            user: result[0]
        })
        //res.send(result[0])        
    })
})

app.post('/users/:id/movies/:movieId', async function (req, res) {    
    const { data } = await axios.get(`http://www.omdbapi.com/?i=${req.params.movieId}&apikey=${apikey}`)
    if (!data['Error']) {
        con.query(`SELECT * FROM users WHERE id=${req.params.id}`, (err, result, fields) => {
            if(result){
                con.query(`INSERT INTO movies_users VALUES (${req.params.id}, '${req.params.movieId}')`, (err, result) => {
                    if (err) throw err
                    console.log("1 record inserted");
                })
            }
        })
    } else {
        res.send(data['Error'])
    }    
})

app.get('/users', function (req, res) {
    if (req.query.limit && req.query.offset) {
        con.query(`SELECT * FROM users LIMIT ${req.query.limit} OFFSET ${req.query.offset}`, (err, result, field) => {
            if (err) throw err
            res.render('pages/index', {
                result: result
            })
        })
    } else if (req.query.order && req.query.reverse) {
        if (req.query.reverse == 1) {
            con.query(`SELECT * FROM users ORDER BY ${req.query.order} DESC`, (err, result, field) => {
                if (err) throw err
                res.render('pages/index', {
                    result: result
                })
            })
        } else {
            con.query(`SELECT * FROM users ORDER BY ${req.query.order} ASC`, (err, result, field) => {
                if (err) throw err
                res.render('pages/index', {
                    result: result
                })
            })
        }

    } else {
        con.query("SELECT * FROM users", (err, result, fields) => {
            if (err) throw err;
            res.render('pages/index', {
                result: result
            })
        })
    }
})

app.delete('/users/:id', function (req, res) {
    con.query(`DELETE FROM users WHERE id=${req.params.id}`, (err, result, field) => {
        if (err) throw err
        res.json(result)
    })
})

app.put('/users/:id', function (req, res) {
    const id = req.params.id

    con.query(`SELECT * FROM users WHERE id=${req.params.id}`, (err, result, field) => {
        if (err) throw err
        if (result) {
            if (req.body.pseudo != null && req.body.email != null && req.body.updatedAt != null) {
                con.query(`UPDATE users SET pseudo='${req.body.pseudo}', email='${req.body.email}', updatedAt='${req.body.updatedAt}' WHERE id=${id}`, (err, result) => {
                    if (err) throw err
                    res.send(result)
                })
            } else if (req.body.pseudo != null && req.body.email != null) {
                con.query(`UPDATE users SET pseudo='${req.body.pseudo}', email='${req.body.email}' WHERE id=${id}`, (err, result) => {
                    if (err) throw err
                    res.send(result)
                })
            } else if (req.body.pseudo != null && req.body.updatedAt != null) {
                con.query(`UPDATE users SET pseudo='${req.body.pseudo}', updatedAt='${req.body.updatedAt}' WHERE id=${id}`, (err, result) => {
                    if (err) throw err
                    res.send(result)
                })
            } else if (req.body.email != null && req.body.updatedAt != null) {
                con.query(`UPDATE users SET email='${req.body.email}', updatedAt='${req.body.updatedAt}' WHERE id=${id}`, (err, result) => {
                    if (err) throw err
                    res.send(result)
                })
            } else if (req.body.pseudo != null) {
                con.query(`UPDATE users SET pseudo='${req.body.pseudo}' WHERE id=${id}`, (err, result) => {
                    if (err) throw err
                    res.send(result)
                })
            } else if (req.body.email != null) {
                con.query(`UPDATE users SET email='${req.body.email}' WHERE id=${id}`, (err, result) => {
                    if (err) throw err
                    res.send(result)
                })
            } else {
                con.query(`UPDATE users SET updatedAt='${req.body.updatedAt}' WHERE id=${id}`, (err, result) => {
                    if (err) throw err
                    res.send(result)
                })
            }
        }
    })
})

app.listen(3000)